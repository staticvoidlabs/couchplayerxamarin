﻿using CouchPlayer.Managers;
using CouchPlayer.Models;
using CouchPlayer.Views;
using System;
using System.Collections.Generic;
using System.Collections.ObjectModel;
using System.ComponentModel;
using System.IO;
using System.Linq;
using System.Net.Http;
using System.Text;
using System.Threading.Tasks;
using Xamarin.Forms;

namespace CouchPlayer
{
    // Learn more about making custom code visible in the Xamarin.Forms previewer
    // by visiting https://aka.ms/xamarinforms-previewer
    [DesignTimeVisible(false)]
    public partial class MainPage : ContentPage
    {
        public ObservableCollection<ShowItem> mShowItems { get; set; }
        public bool mIsButtonbarVisible = false;

        public MainPage()
        {
            InitializeComponent();

            // Fill observalbe collection for listview itemsource.
            mShowItems = new ObservableCollection<ShowItem>();
            
            foreach (var show in RepoManager.GetRepoShowItems())
            {
                mShowItems.Add(show);
            }
            
            listView.ItemsSource = mShowItems;

        }


        // Context menu and button bar selection handler.
        public void OnPlay(object sender, EventArgs e)
        {
            var mi = ((MenuItem)sender);

            DisplayAlert("Play Context Action", mi.CommandParameter + " play context action", "OK");
        }

        public void OnStop(object sender, EventArgs e)
        {
            //var mi = ((MenuItem)sender);
            //DisplayAlert("Stop Context Action", "stop context action", "OK");

            StopPlayback();
        }

        public void OnDelete(object sender, EventArgs e)
        {
            var mi = ((MenuItem)sender);

            DisplayAlert("Delete Context Action", mi.CommandParameter + " delete context action", "OK");
        }

        public void OnBtnStatusClicked(object sender, EventArgs e)
        {

            var tmpNav = Navigation.PushAsync(new StatusPage());
            
        }
        

        // List view item selection handler.
        public void OnSelection(object sender, SelectedItemChangedEventArgs e)
        {
            if (e.SelectedItem == null)
            {
                return; //ItemSelected is called on deselection, which results in SelectedItem being set to null
            }

            //var tmpCell = e.SelectedItem as ViewCell;
            

            // Start playback for selected show.
            ShowItem tmpItem = e.SelectedItem as ShowItem;
            StartPlayback(tmpItem.ShowId);

            //DisplayAlert("Item Selected", e.SelectedItem.ToString(), "Ok");
            //((ListView)sender).SelectedItem = null; //uncomment line if you want to disable the visual selection state.

            //Navigation.PushAsync(new ShowDetailsPage((ShowItem)e.SelectedItem));
        }


        // Show/hide button bar.
        public void OnToolbarToggleClicked(object sender, EventArgs e)
        {
        
            if (mIsButtonbarVisible)
            {
                gridButtons.IsVisible = false;
                mIsButtonbarVisible = false;
            }
            else
            {
                gridButtons.IsVisible = true;
                mIsButtonbarVisible = true;
            }

        }
        


        private async void StartPlayback(int showId)
        {

            try
            {

                // Build Rest request.
                var tmpUri = new Uri(string.Format(@ConnectionManager.mBaseURL + "/play/" + showId));
                //var tmpUri = new Uri(string.Format(@"https://192.168.2.102:5001/api/shows/play/" + showId, string.Empty));

                HttpClientHandler tmpHandler = new HttpClientHandler();
                tmpHandler.ServerCertificateCustomValidationCallback = (message, cert, chain, errors) => true;

                HttpClient tmpClient = new HttpClient(tmpHandler);

                var tmpResponse = await tmpClient.GetAsync(tmpUri);

                if (tmpResponse.IsSuccessStatusCode)
                {
                    //var content = await tmpResponse.Content.ReadAsStringAsync();

                }

            }
            catch (Exception ex)
            {
                throw;
                //DisplayAlert("Error", ex.Message, "OK");
            }

        }

        private async void StopPlayback()
        {

            try
            {

                var tmpUri = new Uri(string.Format(@ConnectionManager.mBaseURL + "/stop"));
                //var tmpUri = new Uri(string.Format(@"https://192.168.2.102:5001/api/shows/stop/"));

                HttpClientHandler tmpHandler = new HttpClientHandler();
                tmpHandler.ServerCertificateCustomValidationCallback = (message, cert, chain, errors) => true;

                HttpClient tmpClient = new HttpClient(tmpHandler);

                var tmpResponse = await tmpClient.GetAsync(tmpUri);

                if (tmpResponse.IsSuccessStatusCode)
                {
                    //var content = await tmpResponse.Content.ReadAsStringAsync();

                }

            }
            catch (Exception ex)
            {
                throw;
                //DisplayAlert("Error", ex.Message, "OK");
            }

        }

    }
}
