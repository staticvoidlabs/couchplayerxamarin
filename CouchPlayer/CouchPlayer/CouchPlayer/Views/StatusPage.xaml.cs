﻿using CouchPlayer.Managers;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Net.Http;
using System.Text;
using System.Threading.Tasks;

using Xamarin.Forms;
using Xamarin.Forms.Xaml;

namespace CouchPlayer.Views
{
    [XamlCompilation(XamlCompilationOptions.Compile)]
    public partial class StatusPage : ContentPage
    {
        public StatusPage()
        {
            InitializeComponent();

            InitStatusPage();

        }


        private async void InitStatusPage()
        {

            try
            {
                
                var tmpUri = new Uri(string.Format(@ConnectionManager.mBaseURL + "/status"));
                //var tmpUri = new Uri(string.Format(@"https://192.168.2.102:5002/api/shows/status", string.Empty));

                HttpClientHandler tmpHandler = new HttpClientHandler();
                tmpHandler.ServerCertificateCustomValidationCallback = (message, cert, chain, errors) => true;

                HttpClient tmpClient = new HttpClient(tmpHandler);

                var tmpResponse = await tmpClient.GetAsync(tmpUri);

                if (tmpResponse.IsSuccessStatusCode)
                {
                    var content = await tmpResponse.Content.ReadAsStringAsync();

                    lblServerStatus.Text = content;
                }
                else
                {
                    lblServerStatus.Text = "Error loading status info!";
                }

            }
            catch (Exception ex)
            {
                lblServerStatus.Text = "Error loading status info!";

                throw;
            }

        }

        public void OnButtonClicked(object sender, EventArgs e)
        {
            Navigation.PopAsync();
        }

    }
}