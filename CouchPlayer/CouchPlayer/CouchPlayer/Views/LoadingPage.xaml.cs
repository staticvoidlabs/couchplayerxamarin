﻿using CouchPlayer.Managers;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

using Xamarin.Forms;
using Xamarin.Forms.Xaml;

namespace CouchPlayer.Views
{
    [XamlCompilation(XamlCompilationOptions.Compile)]
    public partial class LoadingPage : ContentPage
    {

        public RepoManager mRepoManager = new RepoManager();

        public LoadingPage()
        {
            InitializeComponent();

            pckServerList.SelectedIndex = 0;
        }


        public async void OnButtonConnectClicked(object sender, EventArgs e)
        {

            if (pckServerList != null && pckServerList.SelectedItem != null)
            {

                ConnectionManager.mBaseURL = pckServerList.SelectedItem.ToString();

                try
                {
                    lblConnectionLabel.Text = "Connecting...";
                    await mRepoManager.InitRepos();
                }
                catch (Exception ex)
                {
                    lblConnectionLabel.Text = "Connect to server";
                    await Application.Current.MainPage.DisplayAlert("Error", ex.Message, "OK");
                }

                lblConnectionLabel.Text = " Connected!";
                ConnectionManager.mLastConnectionState = "connected";

                Application.Current.MainPage = new NavigationPage(new MainPage());
            }

        }


    }
}