﻿using System;
using System.Collections.Generic;
using System.IO;
using System.Linq;
using System.Threading.Tasks;
using Xamarin.Forms;

namespace CouchPlayer.Models
{
    public class ShowItem
    {

        // Common properties.
        public int ShowId { get; set; }
        public string FullName { get; set; }
        public string Title { get; set; }
        public string TitleShort { get; set; }
        public string TitleDetail { get; set; }
        public string Duration { get; set; }
        public string Origin { get; set; }
        public string OriginShort { get; set; }
        public double OriginLogoScale { get; set; }
        public byte[] ThumbnailByteArray { get; set; }
        public string ThumbnailPath { get; set; }
        public DateTime DateAdded { get; set; }
        public string DateBroadcast { get; set; }
        public string Format { get; set; }
        public int AgeInDays { get; set; }
        
        /*
        public int AgeInDays
        {
            get { return (int)(DateTime.Now - DateAdded).TotalDays; }
        }
        */

        // Xamarin related properties.
        public ImageSource Thumbnail
        {

            get
            {
                Stream stream = new MemoryStream(ThumbnailByteArray);
                var imageSource = ImageSource.FromStream(() => stream);
                return imageSource;
            }
            set { }

        }

        public string OriginLogo
        {

            get
            {
                return OriginShort.ToLower() + ".png";
            }
            set { }

        }


        public string ShowInfosPrimaryLine
        {

            get
            {
                if (Origin.Contains("YouTV"))
                {

                    return TitleShort;
                }
                else
                {
                    return TitleShort;
                }

            }
            set { }

        }

        public string ShowInfosSecondaryLine
        {

            get
            {
                if (Origin.Contains("YouTV"))
                {

                    return Origin + " | Dauer: " + Duration + " | " + DateBroadcast + " | " + AgeInDays + " Tage alt";
                }
                else
                {
                    return Origin + " | Dauer: " + Duration + " | " + AgeInDays + " Tage alt";
                }

            }
            set { }

        }

    }

}
