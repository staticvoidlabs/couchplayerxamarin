﻿using CouchPlayer.Managers;
using CouchPlayer.Views;
using System;
using System.Threading.Tasks;
using Xamarin.Forms;
using Xamarin.Forms.Xaml;

namespace CouchPlayer
{
    public partial class App : Application
    {

        public RepoManager mRepoManager = new RepoManager();


        public App()
        {
            InitializeComponent();

            MainPage = new LoadingPage();
        }


        protected async override void OnStart()
        {
            try 
            {
                //await mRepoManager.InitRepos();
            }
            catch (Exception ex)
            {
                //await MainPage.DisplayAlert("Error", ex.Message, "OK");
            }
            
            //MainPage = new NavigationPage(new MainPage());
        }


        protected override void OnSleep()
        {
            // Handle when your app sleeps
        }


        protected override void OnResume()
        {
            // Handle when your app resumes
        }
    }
}
